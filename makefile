VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
DOCKER ?= docker
PIP ?= $(PYTHON) -m pip
PIP_COMPILE ?= $(VENV)/bin/pip-compile
IMAGE ?= dyff-orchestrator

BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)

.PHONY: all
all:

.PHONY: setup
setup: $(VENV)/requirements.txt

.PHONY: version
version:
	VIRTUAL_ENV=$(VENV) uv pip install -e $(BASE_DIR)

.PHONY: compile
compile:
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt requirements.in

requirements.txt: requirements.in | $(VENV)
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt requirements.in

$(VENV)/requirements.txt: requirements.txt | $(VENV)
	VIRTUAL_ENV=$(VENV) uv pip install -r requirements.txt
	cp -f requirements.txt $(VENV)/requirements.txt

$(VENV):
	uv venv $(VENV)

.PHONY: distclean
distclean:
	rm -rf node_modules/ $(VENV)
	find -name __pycache__ -type d -exec rm -rf '{}' \;
	find -name \*.pyc -type f -exec rm -f '{}' \;

.PHONY: docker-build
docker-build:
	$(DOCKER) build -t $(IMAGE) .

.PHONY: docker-run
docker-run:
	$(DOCKER) run --rm -it $(IMAGE)

.PHONY: docker-run-bash
docker-run-bash:
	$(DOCKER) run --rm -it $(IMAGE) bash

.PHONY: config-listing
config-listing:
	PYTHONPATH=$(shell pwd) $(PYTHON) ./scripts/generate-config-listing.py > docs/configuration.rst
