Configuration
=============

kafka
-----

kafka.config
~~~~~~~~~~~~

kafka.config.bootstrap_servers
``````````````````````````````

Type: ``string``

Default: ``kafka.kafka.svc.cluster.local``

Environment variable: ``DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS``

The address to contact when establishing a connection to Kafka.

.. tabs::

    .. group-tab:: Environment variable

        .. code-block:: bash

            DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS="kafka.kafka.svc.cluster.local"

        .. code-block:: bash

            DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS="kafka.kafka.svc.cluster.local:9093"

kafka.config.compression_type
`````````````````````````````

Type: ``string``

Default: ``zstd``

Environment variable: ``DYFF_KAFKA__CONFIG__COMPRESSION_TYPE``

kafka.topics
~~~~~~~~~~~~

kafka.topics.commands
`````````````````````

Type: ``string``

Environment variable: ``DYFF_KAFKA__TOPICS__COMMANDS``

kafka.topics.workflows_events
`````````````````````````````

Type: ``string``

Default: ``dyff.workflows.events``

Environment variable: ``DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS``

.. tabs::

    .. group-tab:: Environment variable

        .. code-block:: bash

            DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS="test.workflows.events"

kafka.topics.workflows_state
````````````````````````````

Type: ``string``

Default: ``dyff.workflows.state``

Environment variable: ``DYFF_KAFKA__TOPICS__WORKFLOWS_STATE``

.. tabs::

    .. group-tab:: Environment variable

        .. code-block:: bash

            DYFF_KAFKA__TOPICS__WORKFLOWS_STATE="test.workflows.state"

orchestrator
------------

orchestrator.images
~~~~~~~~~~~~~~~~~~~

orchestrator.images.bentoml_service_openllm
```````````````````````````````````````````

Type: ``string``

Default: ``us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/bentoml-service-openllm-runner:latest``

Environment variable: ``DYFF_ORCHESTRATOR__IMAGES__BENTOML_SERVICE_OPENLLM``

orchestrator.images.huggingface
```````````````````````````````

Type: ``string``

Default: ``us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/huggingface-runner:latest``

Environment variable: ``DYFF_ORCHESTRATOR__IMAGES__HUGGINGFACE``

orchestrator.images.mock
````````````````````````

Type: ``string``

Default: ``us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/inferenceservice-mock:latest``

Environment variable: ``DYFF_ORCHESTRATOR__IMAGES__MOCK``

orchestrator.images.standalone
``````````````````````````````

Type: ``string``

Default: ``us-central1-docker.pkg.dev/dyff-354017/dyff-models/{service.id}:latest``

Environment variable: ``DYFF_ORCHESTRATOR__IMAGES__STANDALONE``

orchestrator.images.vllm
````````````````````````

Type: ``string``

Default: ``us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/vllm-runner:latest``

Environment variable: ``DYFF_ORCHESTRATOR__IMAGES__VLLM``

resources
---------

resources.datasets
~~~~~~~~~~~~~~~~~~

resources.datasets.storage
``````````````````````````

resources.datasets.storage.url
''''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__DATASETS__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

resources.inferenceservices
~~~~~~~~~~~~~~~~~~~~~~~~~~~

resources.inferenceservices.storage
```````````````````````````````````

resources.inferenceservices.storage.url
'''''''''''''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__INFERENCESERVICES__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

resources.models
~~~~~~~~~~~~~~~~

resources.models.storage
````````````````````````

resources.models.storage.url
''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__MODELS__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

resources.modules
~~~~~~~~~~~~~~~~~

resources.modules.storage
`````````````````````````

resources.modules.storage.url
'''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__MODULES__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

resources.outputs
~~~~~~~~~~~~~~~~~

resources.outputs.storage
`````````````````````````

resources.outputs.storage.url
'''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__OUTPUTS__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

resources.reports
~~~~~~~~~~~~~~~~~

resources.reports.storage
`````````````````````````

resources.reports.storage.url
'''''''''''''''''''''''''''''

Type: ``string``

Default: ``s3://dyff``

Environment variable: ``DYFF_RESOURCES__REPORTS__STORAGE__URL``

Object storage bucket URL. Must use the s3 protocol.

system
------

system.supports_fuse
~~~~~~~~~~~~~~~~~~~~

Type: ``boolean``

Environment variable: ``DYFF_SYSTEM__SUPPORTS_FUSE``

Does the system support the FUSE interface for object storage?
