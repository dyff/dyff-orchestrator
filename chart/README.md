# dyff-orchestrator

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/dyff-orchestrator)](https://artifacthub.io/packages/search?repo=dyff-orchestrator)

Orchestrator component for the Dyff AI auditing platform.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Installation

```bash
helm install dyff-orchestrator oci://registry.gitlab.com/dyff/charts/dyff-orchestrator
```

## Removal

Removing finalizers is required to delete the associated PVC:

```bash
kubectl patch pvc data-dyff-orchestrator-0 -p '{"metadata":{"finalizers": []}}' --type=merge
```

Now the chart can be deleted:

```bash
helm uninstall dyff-orchestrator
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Set affinity rules. |
| args | list | `["--namespace=default","--local_db_path=/var/dyff/orchestrator/data","--from_beginning","--debug_ignore_bad_messages"]` | Define an argument for the containers that run. |
| command | list | `["python3","-m","dyff.orchestrator"]` | Define a command for the containers that run. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` |  |
| containerSecurityContext.capabilities.drop[0] | string | `"ALL"` |  |
| containerSecurityContext.privileged | bool | `false` |  |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` |  |
| containerSecurityContext.runAsGroup | int | `1001` |  |
| containerSecurityContext.runAsNonRoot | bool | `true` |  |
| containerSecurityContext.runAsUser | int | `1001` |  |
| extraEnvVarsConfigMap | object | `{"DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS":"kafka.kafka.svc.cluster.local","DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS":"dyff.workflows.events","DYFF_KAFKA__TOPICS__WORKFLOWS_STATE":"dyff.workflows.state"}` | Set environment variables in the `dyff-orchestrator` StatefulSet via ConfigMap. |
| extraEnvVarsSecret | object | `{}` | Set environment variables in the `dyff-orchestrator` StatefulSet via Secret. |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` | Set the pull policy. |
| image.repository | string | `"registry.gitlab.com/dyff/dyff-orchestrator"` | Set the repository to pull an image from. |
| image.tag | string | `""` | Override the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` | Set image to pull from private registry. |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` | Set the node labels you want the target node to have. |
| persistence.volumeSpec.accessModes | list | `["ReadWriteOnce"]` | Set access mode. |
| persistence.volumeSpec.resources.requests.storage | string | `"10Gi"` | Set storage size. |
| persistentVolumeClaimRetentionPolicy | object | `{"enabled":true,"whenDeleted":"Retain","whenScaled":"Retain"}` | Set PVC retention policy. |
| podAnnotations | object | `{}` | Set annotations for pods. |
| podLabels | object | `{}` | Set labels for pods. |
| podSecurityContext.fsGroup | int | `1001` | Define the filesystem group ID that owns the volume's files and directories. |
| replicaCount | int | `1` | Set the number of replicas to deploy. |
| resources | object | `{}` | Set container requests and limits for different resources like CPU or memory (essential for production workloads). |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.automount | bool | `true` | Chose to automatically mount a ServiceAccount's API credentials. |
| serviceAccount.create | bool | `true` | Specify if a service account should be created. |
| serviceAccount.name | string | `""` | If not set and create is true, a name is generated using the fullname template. |
| tolerations | list | `[]` | Set the scheduler to schedule pods with matching taints |
| volumeMounts | list | `[]` | Set additional volumeMounts on the output Deployment definition. |
| volumes | list | `[]` | Set additional volumes on the output Deployment definition. |

## License

Copyright 2024 UL Research Institutes.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
