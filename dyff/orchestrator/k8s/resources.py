# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
from __future__ import annotations

import datetime
import enum
from functools import singledispatch
from typing import Any

import kubernetes as k8s

from dyff.schema.base import DyffSchemaBaseModel
from dyff.schema.platform import (
    SYSTEM_ATTRIBUTES,
    Dataset,
    Entities,
    Evaluation,
    InferenceService,
    InferenceSession,
    Model,
    Report,
    Resources,
)

from .. import timestamp
from ..typing import YAMLList, YAMLObject, YAMLScalar, YAMLType

_DYFF_GROUP = "dyff.io"
_DYFF_VERSION = "v1alpha1"
_DYFF_API_VERSION = f"{_DYFF_GROUP}/{_DYFF_VERSION}"


@singledispatch
def to_yaml(spec: Any) -> YAMLType:
    if spec is None:
        return None
    else:
        raise TypeError(f"No conversion defined for {type(spec)}")


@to_yaml.register
def _(spec: datetime.datetime) -> str:
    return timestamp.dt_to_str(spec)


@to_yaml.register
def _(spec: enum.Enum) -> YAMLType:
    return spec.name


@to_yaml.register(str)
def _(spec) -> YAMLScalar:
    # If type uses the Subclass(str, Enum) pattern, singledispatch will detect
    # it as a str.
    if isinstance(spec, enum.Enum):
        return spec.name
    else:
        return spec


@to_yaml.register(int)
@to_yaml.register(float)
@to_yaml.register(bool)
def _(spec) -> YAMLScalar:
    return spec


@to_yaml.register
def _(spec: dict) -> YAMLObject:
    return {k: to_yaml(v) for k, v in spec.items()}


@to_yaml.register
def _(spec: list) -> YAMLList:
    return [to_yaml(x) for x in spec]


@to_yaml.register(Dataset)
@to_yaml.register(Evaluation)
@to_yaml.register(InferenceService)
@to_yaml.register(InferenceSession)
@to_yaml.register(Model)
@to_yaml.register(Report)
def _(spec: DyffSchemaBaseModel) -> YAMLObject:
    d = spec.dict()
    for k in SYSTEM_ATTRIBUTES:
        d.pop(k, None)
    return to_yaml(d)  # type: ignore


_short_names = {
    Entities.Analysis: "analysis",
    Entities.Audit: "audit",
    Entities.Dataset: "data",
    Entities.Evaluation: "eval",
    Entities.InferenceService: "infer",
    Entities.InferenceSession: "session",
    Entities.Model: "model",
    Entities.Report: "report",
}


_component_names = {
    Entities.Analysis: {
        "download": "down",
        "run": "run",
        "upload": "up",
    },
    Entities.Audit: {"fetch": "fetch", "proxy": "proxy", "run": "run"},
    Entities.Dataset: {
        "ingest": "ingest",
    },
    Entities.Evaluation: {
        "client": "client",
        "inference": "infer",
        "verification": "verify",
    },
    Entities.InferenceService: {
        "build": "build",
    },
    Entities.InferenceSession: {
        "inference": "infer",
    },
    Entities.Model: {
        "fetch": "fetch",
    },
    Entities.Report: {
        "download": "down",
        "run": "run",
        "upload": "up",
    },
}


_kind_letters = {
    Entities.Analysis: "a",
    Entities.Dataset: "d",
    Entities.Evaluation: "e",
    Entities.InferenceService: "s",
    Entities.InferenceSession: "i",
    Entities.Model: "m",
    Entities.Report: "r",
}


_component_letters = {
    Entities.Analysis: {
        "download": "d",
        "run": "r",
        "upload": "u",
    },
    Entities.Dataset: {
        "ingest": "f",
    },
    Entities.Evaluation: {
        "client": "c",
        "inference": "i",
        "verification": "v",
    },
    Entities.InferenceService: {
        "build": "b",
    },
    Entities.InferenceSession: {
        "inference": "i",
    },
    Entities.Model: {
        "fetch": "f",
    },
    Entities.Report: {
        "download": "d",
        "run": "r",
        "upload": "u",
    },
}


# def object_name(kind: Entities | str, id: str, component=None) -> str:
#     kind = Entities(kind)
#     # These Dyff kinds are all mapped to the k8s Analysis kind
#     if kind in [Entities.Measurement, Entities.SafetyCase]:
#         kind = Entities.Analysis
#     short_name = _short_names[kind]
#     if component:
#         component_name = _component_names[kind][component]
#         return f"{short_name}-{id}-{component_name}"
#     else:
#         return f"{short_name}-{id}"


def object_name(kind: Entities | str, id: str, component=None) -> str:
    """Returns the k8s name of a Dyff Kubernetes entity.

    Names must be <= 34 characters because of limitations of how Google Cloud
    Platform implements gvisor sandboxing. Since the ID is 32 characters, that
    leaves one prefix letter to indicate the kind, and one suffix letter to
    indicate the component of the workflow.
    """
    kind = Entities(kind)
    # These Dyff kinds are all mapped to the k8s Analysis kind
    if kind in [Entities.Measurement, Entities.SafetyCase]:
        kind = Entities.Analysis
    kind_letter = _kind_letters[kind]
    if component:
        component_letter = _component_letters[kind][component]
        return f"{kind_letter}{id}{component_letter}"
    else:
        return f"{kind_letter}{id}"


def evaluation_name(id: str, component=None) -> str:
    """Returns the k8s name of an evaluation-related component.

    Args:
      component: Could be ``"client"``, ``"inference"``, ``"verification"``, or ``None``
    """
    return object_name(Entities.Evaluation, id, component)


def evaluation_manifest(spec: dict) -> dict:
    return {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "Evaluation",
        "metadata": {
            "name": evaluation_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": Resources.Evaluation.value,
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }


def model_name(id: str, component=None) -> str:
    """Returns the k8s name of a Model-related component.

    Args:
      component: Could be ``"fetch"``, or ``None``
    """
    return object_name(Entities.Model, id, component)


def model_manifest(spec: dict) -> dict:
    return {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "Model",
        "metadata": {
            "name": model_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": Resources.Model.value,
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }


def inference_session_name(id: str, component=None) -> str:
    """Returns the k8s name of an InferenceSession-related component."""
    return object_name(Entities.InferenceSession, id, component)


def inference_session_manifest(spec: dict) -> dict:
    return {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "InferenceSession",
        "metadata": {
            "name": inference_session_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": Resources.InferenceSession.value,
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }


def report_name(id: str, component=None) -> str:
    """Returns the k8s name of a report-related component.

    Args:
      component: Could be ``"run"``, or ``None``
    """
    return object_name(Entities.Report, id, component)


def report_manifest(spec: dict) -> dict:
    return {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "Report",
        "metadata": {
            "name": report_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": Resources.Report.value,
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }


def analysis_name(id: str, component=None) -> str:
    """Returns the k8s name of an analysis-related component.

    Args:
      component: Could be ``"run"``, ``"download"``, ``"upload"``, or ``None``
    """
    return object_name(Entities.Analysis, id, component)


def analysis_manifest(spec: dict) -> dict:
    return {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "Analysis",
        "metadata": {
            "name": analysis_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": Resources.Analysis.value,
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }


def _k8s_kind(kind: Entities | str) -> Entities:
    kind = Entities(kind)
    if kind in [Entities.Measurement, Entities.SafetyCase]:
        kind = Entities.Analysis
    return kind


def create_resource(kind: Entities | str, body, *, namespace: str) -> Any:
    kind = _k8s_kind(kind)
    return k8s.client.CustomObjectsApi().create_namespaced_custom_object(
        group=_DYFF_GROUP,
        version=_DYFF_VERSION,
        namespace=namespace,
        plural=Resources.for_kind(kind).value,
        body=body,
    )


def delete_resource(kind: Entities | str, name: str, *, namespace: str):
    kind = _k8s_kind(kind)
    return k8s.client.CustomObjectsApi().delete_namespaced_custom_object(
        group=_DYFF_GROUP,
        version=_DYFF_VERSION,
        name=name,
        namespace=namespace,
        plural=Resources.for_kind(kind).value,
        # Do we want any non-default options?
        body=k8s.client.V1DeleteOptions(),
    )
