# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
from __future__ import annotations

import datetime
import json
from collections import defaultdict
from typing import NamedTuple, Optional, TypeVar

import absl.app
import absl.flags
import confluent_kafka as kafka
import kubernetes.client.exceptions
import pydantic
from absl import logging

from dyff.schema import commands
from dyff.schema.commands import EntityIdentifier
from dyff.schema.errors import PlatformError
from dyff.schema.platform import (
    Dataset,
    DyffEntity,
    DyffEntityType,
    Entities,
    EntityStatus,
    EntityStatusReason,
    Module,
    Status,
    is_status_terminal,
)

from . import crd
from .config import config
from .k8s import conditions, load_config, resources
from .kafka import deserialize_entity, deserialize_id, serialize_id, serialize_value
from .persistence import EntityMessage, LocalDB, LocalDBWriteBatch

FLAGS = absl.flags.FLAGS

absl.flags.DEFINE_string("namespace", "default", "k8s namespace")

absl.flags.DEFINE_bool("once", False, "Run the polling loop once, then exit")
absl.flags.DEFINE_bool(
    "from_beginning",
    False,
    "Consume the workflows.state topic from the beginning (i.e., reset the commit offset)",
)
absl.flags.DEFINE_bool(
    "disable_kafka_producer", False, "Print log messages but don't modify state"
)
absl.flags.DEFINE_bool(
    "disable_kubernetes_actions",
    False,
    "Don't make changes in Kubernetes. Still update the database and produce messages.",
)
absl.flags.DEFINE_bool("disable_quotas", False, "Don't enforce job quotas")

absl.flags.DEFINE_string("local_db_path", None, "Path to local DB files")

absl.flags.DEFINE_string("workflows_events_topic", None, "Workflows events topic")
absl.flags.DEFINE_string("workflows_state_topic", None, "Workflows state topic")
absl.flags.DEFINE_string("kafka_bootstrap_servers", None, "Address of bootstrap server")

absl.flags.DEFINE_bool(
    "debug_ignore_bad_messages",
    False,
    "Swallow exceptions from process_message(). Useful for testing where there are messages with obsolete schemas still in the topic.",
)
absl.flags.DEFINE_bool(
    "debug_ignore_missing_dependencies",
    False,
    "Assume that dependencies that are not in the dependency graph are in a Ready status.",
)


# FIXME: Should have per-account quotas stored in the database
quotas: dict[str, int] = {
    Entities.Analysis: 8,
    Entities.Audit: 8,
    Entities.Dataset: 1,
    Entities.Evaluation: 4,
    Entities.InferenceService: 4,
    Entities.InferenceSession: 4,
    Entities.Measurement: 8,
    Entities.Method: 8,
    Entities.Model: 4,
    Entities.Module: 1,
    Entities.Report: 8,
    Entities.SafetyCase: 4,
    Entities.UseCase: 8,
}


def quota(kind: str) -> int:
    return quotas.get(kind, 1)


_DyffEntityT = TypeVar("_DyffEntityT", bound=DyffEntity)


class JobQueueItem(NamedTuple):
    """Metadata about jobs that are in the job queue. The sort order of the
    queue is the sort order of the JobQueueItem type.
    """

    # This should be 0 for admitted entities and 1 otherwise, so that admitted
    # entities are sorted before not-admitted entities.
    waiting: bool
    creationTime: datetime.datetime
    # .id is included as a tiebreaker in case .creationTime's are equal, since
    # DyffEntity's don't have an ordering defined
    id: str
    entity: EntityMessage

    @staticmethod
    def from_entity(entity: EntityMessage) -> JobQueueItem:
        return JobQueueItem(
            waiting=(entity.entity.status != EntityStatus.admitted),
            creationTime=entity.entity.creationTime,
            id=entity.entity.id,
            entity=entity,
        )


class JustTopicPartition(NamedTuple):
    """The kafka.TopicPartition class has lots of other stuff in it, too.
    This is just the topic and partition.
    """

    topic: str
    partition: int


class ForgetLiveEntityError(PlatformError):
    pass


class KeyMismatchError(PlatformError):
    pass


class Orchestrator:
    """Manages Dyff entities that are in the ``Created`` status until advancing
    them to the ``Admitted`` status.

    The Orchestrator maintains a local snapshot of entity state in a RocksDB
    instance. Incoming Kafka messages update the local snapshot. Periodically,
    the Orchestrator runs a scheduling task using its local snapshot of entity
    state.

    The Orchestrator also maintains a dependency graph, which is also stored
    in RocksDB. This records which workflows are waiting on dependencies to
    complete.
    """

    def __init__(
        self,
        *,
        local_db_path: str,
        workflows_events_topic: str,
        workflows_state_topic: str,
        kafka_bootstrap_servers: str,
        disable_kafka_producer: bool = False,
        disable_kubernetes_actions: bool = False,
    ):
        self.workflows_events_topic = workflows_events_topic
        self.workflows_state_topic = workflows_state_topic

        self.db = LocalDB(local_db_path)
        self.dependency_graph = self.db.get_dependency_graph()
        # The offsets stored in Kafka's __consumer_offsets topic at the time
        # that the orchestrator subscribes to the state topic. Populated by
        # Orchestrator._subscribe().
        self.committed_offsets_at_startup: dict[JustTopicPartition, int] = dict()
        self.disable_kafka_producer = disable_kafka_producer
        self.disable_kubernetes_actions = disable_kubernetes_actions

        consumer_config = {
            "bootstrap.servers": kafka_bootstrap_servers,
            "group.id": "dyff.system.orchestrator",
            "auto.offset.reset": "earliest",
            "enable.auto.commit": False,
            "on_commit": self.on_commit,
        }
        self.consumer = kafka.Consumer(consumer_config)

        producer_config = {
            "bootstrap.servers": kafka_bootstrap_servers,
            "client.id": "dyff.system.orchestrator",
            # This is the default, but setting it explicitly means an exception will
            # be raised if other config settings are in conflict.
            "enable.idempotence": True,
        }
        self.producer = kafka.Producer(producer_config)

        self.log_debug_db()

    def log_debug_db(self):
        if logging.level_debug():
            db_lines = []
            for k, v in self.db.db.items():
                key = k.decode()
                if key != "__dependency_graph":
                    record = json.loads(v.decode())
                    entity = record["entity"]
                    db_lines.append(
                        f"{k.decode()}: {record['offset']} {entity['kind']} {entity['status']} {entity.get('reason')}"
                    )
            db_str = "\n".join(db_lines)
            logging.debug(f"===== LocalDB:\n{db_str}")
            logging.debug(f"----- DependencyGraph:\n{self.dependency_graph}")

    def close(self):
        if self.producer:
            self.producer.flush()
        if self.consumer:
            self.consumer.close()
        if self.db:
            self.db.close()

    def _admit_entity(self, entity: _DyffEntityT, manifest) -> Status:
        assert entity.status == EntityStatus.created

        conditions_list = conditions.ConditionsList()
        conditions_list.set(
            EntityStatus.admitted, status=conditions.ConditionStatus.true
        )
        manifest["status"] = {"conditions": conditions_list.conditions_list}

        if not self.disable_kubernetes_actions:
            resources.create_resource(entity.kind, manifest, namespace=FLAGS.namespace)
        logging.info(f"created {manifest['metadata']['name']}")
        return Status(status=EntityStatus.admitted, reason=None)

    def on_produce_acknowledged(self, error, msg):
        if error:
            logging.error(f"delivery failed: {error}")
        else:
            logging.debug(f"message produced: {msg}")

    def on_commit(self, error, partitions):
        if error:
            logging.error(f"commit failed: {error}")
        else:
            logging.debug(f"committed partition offsets: {partitions}")

    def create_k8s_workflow(self, entity: DyffEntityType) -> Optional[Status]:
        """Possibly admits a workflow by creating the corresponding k8s
        resource. Returns workflow status, or None if no action was taken.
        """
        try:
            manifest = crd.manifest(entity)
            if manifest is not None:
                return self._admit_entity(entity, manifest)
            else:
                logging.debug(f"Nothing to do for {entity.kind} {entity.id}")
                if isinstance(entity, Dataset):
                    # No status change: user needs to upload files and then
                    # manually indicate Ready
                    return None
                elif isinstance(entity, Module):
                    # No status change: user needs to upload files and then
                    # manually indicate Ready
                    return None
                else:
                    return Status(status=EntityStatus.ready, reason=None)
        except Exception:
            logging.exception(
                f"workflow {entity.kind} {entity.id}: failed to create k8s manifest"
            )
            # TODO: Make an enum entry for SchemaError reason (?)
            return Status(status=EntityStatus.failed, reason="SchemaError")

    def delete_k8s_workflow(self, entity: DyffEntityType):
        """Deletes a k8s workflow resource."""
        # This is how we know if a k8s entity was created
        # (see: create_k8s_workflow())
        if not crd.manifest(entity):
            return
        name = resources.object_name(entity.kind, entity.id)
        if not self.disable_kubernetes_actions:
            resources.delete_resource(entity.kind, name, namespace=FLAGS.namespace)
        logging.info(f"deleted {name}")

    def produce_UpdateEntityStatus(self, entity: EntityIdentifier, update: Status):
        command = commands.UpdateEntityStatus(
            data=commands.UpdateEntityStatusData(
                id=entity.id,
                kind=entity.kind,
                attributes=commands.UpdateEntityStatusAttributes(
                    status=update.status,
                    reason=update.reason,
                ),
            )
        )
        return self._produce_event(entity.id, command)

    def _produce_event(self, entity_id: str, command: commands.Command):
        """Produce to the workflows.events topic. The update must be a partial
        entity spec that is json-mergeable with the full spec.
        """
        json_command = command.model_dump(mode="json")
        logging.debug(f"produce: {entity_id}: {command}")
        if not self.disable_kafka_producer:
            self.producer.produce(
                self.workflows_events_topic,
                key=serialize_id(entity_id),
                value=serialize_value(json_command),
                callback=self.on_produce_acknowledged,
            )

    def maybe_admit_more_workflows(self) -> None:
        """Scan the dependency graph for workflows whose dependencies are
        satisfied, and either admit them or place them in the
        ``.reason == QuotaLimit`` state as appropriate.

        Note that we do not modify the local db here; instead we send events
        and let the updates happen via the event processing logic. This is
        because events might change the dependency graph and we want to keep
        those updates in one place for simplicity.
        """
        # account -> kind -> [JobQueueItem]
        schedulable_entities: dict[str, dict[str, list[JobQueueItem]]] = defaultdict(
            lambda: defaultdict(list)
        )
        for schedulable_id in self.dependency_graph.schedulable_entities():
            msg = self.db.get_entity(schedulable_id)
            # Entity can be None if it is mentioned as a dependency but we haven't
            # received the Created message yet
            if msg is not None:
                # [DYFF-381] There is a race condition when the 'status=Ready'
                # message comes from outside the orchestrator, as is the case
                # for workflows where the user uploads data and then calls
                # .finalize(). We work around this by marking those workflows
                # with 'reason=WaitingForUpload' and not putting them through
                # the scheduling process.
                schedulable = False
                if msg.entity.status == EntityStatus.admitted:
                    # Put these in the queue so that they count towards the
                    # quota limit
                    schedulable = True
                elif msg.entity.status == EntityStatus.created:
                    # TODO: Add 'WaitingForUpload' to EntityStatusReason
                    if msg.entity.reason not in ["WaitingForUpload"]:
                        schedulable = True

                if schedulable:
                    schedulable_entities[msg.entity.account][msg.entity.kind].append(
                        JobQueueItem.from_entity(msg)
                    )

        for account, kinds in schedulable_entities.items():
            for kind, entities in kinds.items():
                # See JobQueueItem for ordering:
                #   * .status == Admitted items come before .status == Created items
                #   * Items with same status ordered by .creationTime
                entities.sort()
                for i, item in enumerate(entities):
                    entity = item.entity.entity
                    if not item.waiting:
                        continue
                    if FLAGS.from_beginning and not self.is_fresh_entity_message(
                        item.entity
                    ):
                        # Kafka consumer is not caught up to last committed
                        # state of the entity
                        logging.debug(f"stale entity: {item.entity}")
                        continue

                    if i < quota(kind) or FLAGS.disable_quotas:
                        # TODO: per-account quotas

                        # Kubernetes is idempotent to multiple delivery because the jobs
                        # will have the same name and k8s will refuse to create the
                        # duplicate jobs, so k8s effectively turns at-least-once execution
                        # of the scheduling action into exactly-once execution.
                        try:
                            update = self.create_k8s_workflow(entity)
                        except Exception:
                            logging.exception("admission failed")
                            # TODO: Add enum entry for AdmissionFailed
                            update = Status(
                                status=EntityStatus.error, reason="AdmissionFailed"
                            )
                            if not FLAGS.debug_ignore_bad_messages:
                                raise
                        if update is not None:
                            # Failure here =>
                            #   * Next scheduling pass sees schedulable entity
                            #   * Double resource creation fails due to duplicate job name
                            #   => OK
                            self.produce_UpdateEntityStatus(
                                EntityIdentifier.of(entity), update
                            )
                            # Failure here =>
                            #   * Next scheduling pass sees schedulable entity
                            #   * Double resource creation fails due to duplicate job name
                            #   * Double event produced but Orchestrator will ignore it
                            #   => OK
                    elif entity.reason != EntityStatusReason.quota_limit:
                        # QuotaLimit
                        # The if-condition just reduces the number of redundant events
                        update = Status(
                            status=EntityStatus.created,
                            reason=EntityStatusReason.quota_limit,
                        )
                        self.produce_UpdateEntityStatus(
                            EntityIdentifier.of(entity), update
                        )
                        # Failure here =>
                        #   * Next scheduling pass sees blocked entity
                        #   * Double event produced but Orchestrator will ignore it
                        #   => OK

    def deserialize_message(self, msg: kafka.Message) -> tuple[str, DyffEntityType]:
        id = deserialize_id(msg.key())
        entity = deserialize_entity(msg.value())
        return id, entity

    def process_message(
        self, msg: kafka.Message, *, key: str, entity: DyffEntityType | None
    ):
        """Update the local RocksDB in response to an incoming Kafka message.

        The state of the entity in the message is always updated to keep the local
        snapshot in sync with the Kafka topic.

        The dependency graph is also updated in the following two cases:
          * A brand new entity in Created status (i.e., .reason is None)
          * An entity in a terminal status (finished or failed)

        No actions are taken here that modify the state of the system outside of
        the Orchestrator. Scheduling actions occur separately in
        ``maybe_admit_more_workflows()``, which runs periodically on a timer. The
        admission process may require significant computation if there are a lot
        of pending jobs, so executing it on every event would be excessive.
        """

        # To ensure fault tolerance, we need to make scheduling decisions based on
        # the current state, not on state transitions (similar to how the k8s
        # operator is implemented).
        #
        # Recording the state in the local DB is semantically similar to commiting
        # the consumer position in Kafka, i.e., it needs to happen *after* taking
        # any other actions so we have at-least-once execution of those actions.

        logging.info(f"process_message(): {key}: {msg.value()}")

        stored_entity = self.db.get_entity(key)

        # RocksDB batch write happens in a single transaction
        write_batch = LocalDBWriteBatch()
        should_act = self.is_fresh_message(msg)
        if not should_act:
            logging.debug(f"replayed message: {key}")

        if entity is None:
            # Tombstone: Delete entity data from internal storage
            if stored_entity is not None:
                if stored_entity.entity.status != EntityStatus.deleted:
                    raise ForgetLiveEntityError(
                        f"message key {key}: received tombstone but status was"
                        f" {stored_entity.entity.status}; should be {EntityStatus.deleted}"
                    )
                logging.debug(f"forgetting {key}")
                write_batch.delete_entity(key)
        else:
            # entity = deserialize_entity(msg.value())
            if key != entity.id:
                raise KeyMismatchError(
                    f"message key {key} != entity.id {entity.id}; skipping message"
                )

            # Short-circuit for some duplicate messages that are easy to detect.
            # This doesn't catch all double message deliveries, because failure between
            # db.put() and consumer.commit() leaves db ahead of consumer.
            if stored_entity and stored_entity.entity == entity:
                logging.debug(f"rejected duplicate message: {entity}")
                return

            # Store the new entity state
            write_batch.put_entity(
                entity.id,
                EntityMessage(
                    topic=msg.topic(),
                    partition=msg.partition(),
                    offset=msg.offset(),
                    entity=entity,
                ),
            )

            # These are the status events that might change the dependency graph
            if entity.status == EntityStatus.created and entity.reason is None:
                update = self.dependency_graph.add_created(self.db, entity)
                write_batch.put_dependency_graph(self.dependency_graph)
                if should_act and update:
                    self.produce_UpdateEntityStatus(EntityIdentifier.of(entity), update)
            elif is_status_terminal(entity.status):
                if should_act and entity.status in [
                    EntityStatus.deleted,
                    EntityStatus.terminated,
                ]:
                    # TODO: We could delete all k8s entities that are in terminal
                    # states, but it's useful to keep them around for debugging.
                    # Alternatively, we could pass the Terminated status through
                    # to the operator, so it can delete child workflows but leave
                    # the parent workflow in k8s for examination.
                    try:
                        self.delete_k8s_workflow(entity)
                    except kubernetes.client.exceptions.ApiException as ex:
                        # NotFound is OK because we might have a duplicate message
                        # or a user might delete the entity after the k8s resource
                        # has been GC'd.
                        if ex.status != 404:
                            raise
                changed_dependents: list[DyffEntityType] = (
                    self.dependency_graph.on_workflow_termination(self.db, entity)
                )
                for e in changed_dependents:
                    if should_act:
                        # Let these changes happen via events rather than writing
                        # directly: we want to have the right message offset.
                        self.produce_UpdateEntityStatus(
                            EntityIdentifier.of(e),
                            Status(status=e.status, reason=e.reason),
                        )
                write_batch.put_dependency_graph(self.dependency_graph)

        # DB write is last step so that we get at-least-once execution of the
        # dependency graph update.
        self.db.write_batch(write_batch)
        # Failure here =>
        #   * State msg received again
        #   * (Possible) Event msg produced again but orchestrator will ignore
        #   * Double put in DB but the value is the same
        #   => OK
        self.log_debug_db()

    def _subscribe(self, topics: list[str], *, from_beginning: bool) -> None:
        def on_assign(consumer, partitions):
            # Record the last committed offsets for the consumer. We use these
            # to detect stale messages when replaying the topic to rebuild
            # the local DB.
            committed = consumer.committed(partitions)
            for tp in committed:
                just_tp = JustTopicPartition(topic=tp.topic, partition=tp.partition)
                self.committed_offsets_at_startup[just_tp] = tp.offset
            # If reading from beginning, reset all partition offsets
            if from_beginning:
                for partition in partitions:
                    # Note: -2 is the Kafka special value meaning "from beginning"
                    partition.offset = -2
            # This is what the default on_assign does
            consumer.assign(partitions)

        self.consumer.subscribe(topics, on_assign=on_assign)

    def is_fresh_offset(self, *, topic: str, partition: int, offset: int) -> bool:
        """Returns True if the orchestrator had not yet committed the message
        offset at the time it subscribed to the workflows.state topic. This
        means that the previous run of the orchestrator may not have acted on
        the message yet, so this instance should.
        """
        try:
            just_tp = JustTopicPartition(topic=topic, partition=partition)
            # "By convention, committed offsets reflect the next message to be
            #  consumed, *not* the last message consumed."
            # See: https://docs.confluent.io/platform/current/clients/confluent-kafka-python/html/index.html#confluent_kafka.Consumer.commit
            # => offset = starup_offset is the first fresh message
            return offset >= self.committed_offsets_at_startup[just_tp]
        except KeyError:
            # The function was called before the first message arrived
            return False

    def is_fresh_message(self, msg: kafka.Message) -> bool:
        return self.is_fresh_offset(
            topic=msg.topic(), partition=msg.partition(), offset=msg.offset()
        )

    def is_fresh_entity_message(self, entity: EntityMessage) -> bool:
        return self.is_fresh_offset(
            topic=entity.topic, partition=entity.partition, offset=entity.offset
        )

    def run(self, *, once: bool = False):
        """Run the Orchestrator loop forever.

        If ``once == True``, return after running the loop once, including one run
        of the scheduling step.
        """

        def datetime_now() -> datetime.datetime:
            return datetime.datetime.now(datetime.timezone.utc)

        scheduler_interval = datetime.timedelta(seconds=10)
        topics = [self.workflows_state_topic]

        self._subscribe(topics, from_beginning=FLAGS.from_beginning)

        # This starts as None so that scheduling always runs on the first iteration,
        # otherwise the --once flag would be fairly useless
        then = None
        while True:
            # Kafka messages update the local DB state, but we only act on them
            # periodically
            msg = self.consumer.poll(timeout=1.0)
            if msg is not None:
                if msg.error():
                    if msg.error().code() == kafka.KafkaError._PARTITION_EOF:
                        logging.warning(
                            f"{msg.topic()}:{msg.partition()} EOF at offset {msg.offset()}"
                        )
                    else:
                        raise kafka.KafkaException(msg.error())
                else:
                    # TODO: If msg.offset() <= initial_offsets[msg.partition()]
                    # then we're still rebuilding the local DB. We need to not
                    # take any actions that change the cluster state until
                    # every partition is caught up. That includes admitting
                    # workflows and producing events.

                    if logging.level_debug():
                        logging.debug(
                            f"startup_offsets: {self.committed_offsets_at_startup}"
                        )
                        logging.debug(f"message offset: {msg.offset()}")
                        logging.debug(
                            f"committed offset: {self.consumer.committed([kafka.TopicPartition(msg.topic(), msg.partition())])}"
                        )

                    id = deserialize_id(msg.key())
                    entity: DyffEntityType | None = None
                    try:
                        # Want "at least once" delivery of state message
                        #   => "process" must happen before "commit"
                        entity = (
                            deserialize_entity(msg.value())
                            if msg.value() is not None
                            else None
                        )
                        self.process_message(msg, key=id, entity=entity)
                    except pydantic.ValidationError as ex:
                        logging.exception(f"validation error: {msg.value()}")
                        if not FLAGS.debug_ignore_bad_messages:
                            raise
                    except Exception:
                        logging.exception(f"bad message: {msg.value()}")
                        if not FLAGS.debug_ignore_bad_messages:
                            raise
                    # Failure here =>
                    #   * DB update happens again
                    #   => OK (see process_message)
                    self.consumer.commit(asynchronous=True)

            # Periodically check if we can schedule more stuff
            now = datetime_now()
            if (then is None) or (now - then >= scheduler_interval):
                logging.debug("started admission pass")
                try:
                    self.maybe_admit_more_workflows()
                except pydantic.ValidationError:
                    logging.error(f"schema error in DB")
                    if not FLAGS.debug_ignore_bad_messages:
                        raise
                then = now
                logging.debug("finished admission pass")

            if once:
                break


def main(_unused_argv):
    load_config()

    orchestrator = Orchestrator(
        local_db_path=FLAGS.local_db_path,
        workflows_events_topic=(
            FLAGS.workflows_events_topic or config.kafka.topics.workflows_events
        ),
        workflows_state_topic=(
            FLAGS.workflows_state_topic or config.kafka.topics.workflows_state
        ),
        kafka_bootstrap_servers=(
            FLAGS.kafka_bootstrap_servers or config.kafka.config.bootstrap_servers
        ),
        disable_kafka_producer=FLAGS.disable_kafka_producer,
        disable_kubernetes_actions=FLAGS.disable_kubernetes_actions,
    )
    try:
        orchestrator.run(once=FLAGS.once)
    finally:
        orchestrator.close()
