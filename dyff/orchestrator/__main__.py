# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import absl.app

from .main import main

if __name__ == "__main__":
    absl.app.run(main)
