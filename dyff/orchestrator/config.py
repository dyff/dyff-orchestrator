# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import os
from typing import List, Optional

from pydantic import BaseModel, BaseSettings, Field, SecretStr
from pydantic.fields import Undefined


class KafkaConfigConfig(BaseModel):
    bootstrap_servers: str = Field(
        default="kafka.kafka.svc.cluster.local",
        description="The address to contact when establishing a connection to Kafka.",
        examples=[
            "kafka.kafka.svc.cluster.local",
            "kafka.kafka.svc.cluster.local:9093",
        ],
    )

    compression_type: str = "zstd"

    def get_producer_config(self, keys: Optional[List[str]] = None):
        if keys is None:
            keys = ["bootstrap.servers", "compression.type"]
        return {key: self.get_by_kafka_key(key) for key in keys}

    def get_by_kafka_key(self, key: str):
        field_name = key.replace(".", "_")
        return getattr(self, field_name)


class KafkaTopicsConfig(BaseModel):
    commands: str = None
    workflows_events: str = Field(
        default="dyff.workflows.events",
        examples=["test.workflows.events"],
    )
    workflows_state: str = Field(
        default="dyff.workflows.state",
        examples=["test.workflows.state"],
    )


class KafkaConfig(BaseModel):
    config: KafkaConfigConfig = Field(default_factory=KafkaConfigConfig)
    topics: KafkaTopicsConfig = Field(default_factory=KafkaTopicsConfig)


def mongodb_connection_string_field(default: Optional[str] = None) -> SecretStr:
    if default is None:
        default = ("mongodb://localhost:27017/&ssl=false",)
    return Field(
        description=(
            """Set the MongoDB connection string, following this pattern::

    mongodb+srv://[username:password@]host[/[defaultauthdb][?options]]

For more info, see the `MongoDB manual
<https://www.mongodb.com/docs/manual/reference/connection-string/>`_.
"""
        ),
        default=default,
        examples=[
            "mongodb+srv://USER:PASS@dyff-datastore-rs0.mongodb.svc.cluster.local/workflows?replicaSet=rs0&ssl=false&authSource=users",  # pragma: allowlist secret
        ],
    )


def mongodb_database_field(default=Undefined) -> str:
    return Field(
        description=("""Name of the MongoDB database to connect to."""),
        default=default,
        examples=[
            "accounts",
            "workflows",
        ],
    )


class StorageConfigV2(BaseModel):
    url: str = Field(
        description="""Object storage bucket URL. Must use the s3 protocol.""",
        default="s3://dyff",
    )


class DataSetsConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/datasets")
    )


class InferenceServicesConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/inferenceservices")
    )


class ModelsConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/models")
    )


class ModulesConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/modules")
    )


class OutputsConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/outputs")
    )


class ReportsConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(url="s3://dyff/reports")
    )


class ResourcesConfig(BaseModel):
    datasets: DataSetsConfig = Field(default_factory=DataSetsConfig)
    inferenceservices: InferenceServicesConfig = Field(
        default_factory=InferenceServicesConfig
    )
    models: ModelsConfig = Field(default_factory=ModelsConfig)
    modules: ModulesConfig = Field(default_factory=ModulesConfig)
    outputs: OutputsConfig = Field(default_factory=OutputsConfig)
    reports: ReportsConfig = Field(default_factory=ReportsConfig)


class OrchestratorImageTemplates(BaseModel):
    bentoml_service_openllm: str = (
        "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/bentoml-service-openllm-runner:latest"
    )
    huggingface: str = (
        "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/huggingface-runner:latest"
    )
    mock: str = (
        "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/inferenceservice-mock:latest"
    )
    standalone: str = (
        "us-central1-docker.pkg.dev/dyff-354017/dyff-models/{service.id}:latest"
    )
    vllm: str = (
        "us-central1-docker.pkg.dev/dyff-354017/dyff-system/ul-dsri/dyff/dyff/vllm-runner:latest"
    )


class SystemConfig(BaseModel):
    supports_fuse: bool = Field(
        default=False,
        description="Does the system support the FUSE interface for object storage?",
    )


class OrchestratorConfig(BaseModel):
    # image_pull_secrets: ImagePullSecret = Field(default_factory=list)
    images: OrchestratorImageTemplates = Field(
        default_factory=OrchestratorImageTemplates
    )


class DyffConfig(BaseSettings):
    kafka: KafkaConfig = Field(default_factory=KafkaConfig)
    orchestrator: OrchestratorConfig = Field(default_factory=OrchestratorConfig)
    resources: ResourcesConfig = Field(default_factory=ResourcesConfig)
    system: SystemConfig = Field(default_factory=SystemConfig)

    class Config:
        env_file = os.environ.get("DYFF_DOTENV_FILE", ".env")
        # Env variables start with 'DYFF_'
        env_prefix = "dyff_"
        # Env var like 'DYFF_KAFKA__FOO' will be stored in 'kafka.foo'
        env_nested_delimiter = "__"


config = DyffConfig()
