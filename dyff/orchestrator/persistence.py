# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import json
from typing import Iterable, Optional, TypeVar

import networkx
import rocksdict

from dyff.schema.platform import (
    DyffEntity,
    DyffEntityType,
    DyffSchemaBaseModel,
    EntityStatus,
    EntityStatusReason,
    Status,
    is_status_success,
    is_status_terminal,
)


class EntityMessage(DyffSchemaBaseModel):
    topic: str
    partition: int
    offset: int
    entity: DyffEntityType


class LocalDBWriteBatch:
    """Represents a batch write operation.

    Apply the batch write with ``LocalDB.write_batch()``. The batch write happens
    in a single transaction.
    """

    def __init__(self):
        self.batch = rocksdict.WriteBatch(raw_mode=True)

    def put_entity(self, id: str, entity: EntityMessage):
        """Add an entity put operation to the batch."""
        self.batch.put(id.encode("utf-8"), entity.json().encode("utf-8"))

    def delete_entity(self, key: str):
        self.batch.delete(key.encode("utf-8"))

    def put_dependency_graph(self, dependency_graph: DependencyGraph):
        """Add a DependencyGraph put operation to the batch."""
        self.batch.put(
            "__dependency_graph".encode("utf-8"), dependency_graph.serialize()
        )


class LocalDB:
    """A local database that stores the Orchestrator's snapshot of the state of
    all the workflow entities in the system as well as the current dependency
    graph.
    """

    def __init__(self, db_path: str):
        self.db = rocksdict.Rdict(db_path, options=rocksdict.Options(raw_mode=True))

    def close(self):
        if self.db:
            self.db.close()

    def __getitem__(self, id: str) -> Optional[EntityMessage]:
        return self.get_entity(id)

    def get_entity(self, id: str) -> Optional[EntityMessage]:
        """Get an entity by ID."""
        raw = self.db.get(id.encode("utf-8"))
        return raw and EntityMessage.parse_raw(raw)

    def get_dependency_graph(self) -> DependencyGraph:
        """Get the dependency graph."""
        graph_bytes: Optional[bytes] = self.db.get("__dependency_graph".encode("utf-8"))
        graph = DependencyGraph()
        if graph_bytes is None:
            # This doesn't need to be part of a batch transaction because the
            # graph is empty.
            self.db.put("__dependency_graph".encode("utf-8"), graph.serialize())
        else:
            graph.deserialize(graph_bytes)
        return graph

    def write_batch(self, batch: LocalDBWriteBatch):
        """Execute a batch write.

        The batch write happens in a single transaction, so it either totally
        succeeds or it fails without changing the DB state.
        """
        return self.db.write(batch.batch)


_DyffEntityT = TypeVar("_DyffEntityT", bound=DyffEntity)


class DependencyGraph:
    """A directed graph representing dependencies among workflows. There is
    an edge JobA -> JobB if JobB depends on JobA.
    """

    def __init__(self, *, ignore_missing_dependencies: bool = False):
        self._graph = networkx.DiGraph()
        self._ignore_missing_dependencies = ignore_missing_dependencies

    def add_created(self, db: LocalDB, entity: _DyffEntityT) -> Optional[Status]:
        """Add a newly-created workflow to the graph.

        Returns either the original entity unchanged, or a copy of the entity with
        its .status and .reason fields set appropriately. The returned entity
        may have .reason == UnsatisfiedDependency if it is waiting for a dependency
        to complete, or .status == Error and .reason == FailedDependency if one
        of its dependencies is already in a failure state.
        """
        assert entity.status == EntityStatus.created
        assert entity.reason == None
        edges = []
        update: Optional[Status] = None
        for dependency_id in entity.dependencies():
            dependency = db.get_entity(dependency_id)
            if dependency is None:
                if not self._ignore_missing_dependencies:
                    edges.append((dependency_id, entity.id))
                    update = Status(
                        status=EntityStatus.created,
                        reason=EntityStatusReason.unsatisfied_dependency,
                    )
            elif not is_status_terminal(dependency.entity.status):
                edges.append((dependency_id, entity.id))
                update = Status(
                    status=EntityStatus.created,
                    reason=EntityStatusReason.unsatisfied_dependency,
                )
            elif not is_status_success(dependency.entity.status):
                # We treat this as an unrecoverable error
                update = Status(
                    status=EntityStatus.error,
                    reason=EntityStatusReason.failed_dependency,
                )
                break
        else:
            # Delay graph changes until here in case there's a failed dependency
            self._graph.add_node(entity.id)
            self._graph.add_edges_from(edges)
        return update

    def on_workflow_termination(
        self, db: LocalDB, entity: DyffEntityType
    ) -> list[DyffEntityType]:
        """Update the graph when a workflow terminates.

        Returns a list of entities whose .status and/or .reason also changed as
        a result of the termination. The returned list *does not* include the
        ``entity`` argument.

        Currently, the only reason that other entities' state
        would change is if the workflow terminated in a failure state and other
        workflows depend on it.
        """

        def propagate_failure(root: DyffEntityType) -> list[DyffEntityType]:
            failures: list[DyffEntityType] = []
            dfs_stack: list[str] = [root.id]
            visited = set()
            while dfs_stack:
                current_node = dfs_stack.pop()
                if current_node in visited:
                    continue
                visited.add(current_node)
                dfs_stack.extend(self._graph.successors(current_node))
                if current_node != root.id:
                    # Don't update the status of the root entity, to preserve its .reason
                    current_entity = db.get_entity(current_node)
                    assert current_entity is not None
                    update = {
                        "status": EntityStatus.error,
                        "reason": EntityStatusReason.failed_dependency,
                    }
                    failures.append(current_entity.entity.copy(update=update))

            for failure in failures:
                self._graph.remove_node(failure.id)
            return failures

        assert is_status_terminal(entity.status)
        if not self._graph.has_node(entity.id):
            # If we receive a duplicate message, we may have already removed the node
            return []

        failures = []
        if not is_status_success(entity.status):
            failures = propagate_failure(entity)
        self._graph.remove_node(entity.id)
        return failures

    def schedulable_entities(self) -> Iterable[str]:
        """Yields the .id's of entities in the graph that have all of their
        dependencies satisfied.
        """
        yield from (id for id, in_degree in self._graph.in_degree() if in_degree == 0)

    def deserialize(self, data: bytes):
        """Restore the graph structure from its serialized representation."""
        graph_data = json.loads(data.decode("utf-8"))
        self._graph = networkx.readwrite.json_graph.adjacency_graph(
            graph_data, directed=True, multigraph=False
        )

    def serialize(self) -> bytes:
        """Returns the serialized representation of the graph structure."""
        graph_data = networkx.readwrite.json_graph.adjacency_data(self._graph)
        return json.dumps(graph_data).encode("utf-8")

    def __str__(self) -> str:
        graph_data = networkx.readwrite.json_graph.adjacency_data(self._graph)
        return json.dumps(graph_data, indent=2)
